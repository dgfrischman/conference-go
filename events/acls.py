import requests, json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):

     # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Make the request
    params ={
        "query": f"{city} {state}",
        "per_page": 1
    }
    response = requests.get(url, headers=headers, params=params)
    # Parse the JSON response
    city_pic = response.json()

    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    pic_url = {
        "picture_url": city_pic['photos'][0]['src']['original']
    }

    # Return the dictionary
    return pic_url

def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    geo_response = requests.get(geo_url)
    # Parse the JSON response
    geocode = geo_response.json()
    # Get the latitude and longitude from the response
    lat = geocode[0]['lat']
    lon = geocode[0]['lon']
    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # Make the request
    weather_response = requests.get(weather_url)
    # Parse the JSON response
    weather = weather_response.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    curr_weather = {
        "temperature": weather['main']['temp'],
        "description": weather['weather'][0]['description']
    }
    # Return the dictionary
    return curr_weather
