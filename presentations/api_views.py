from django.http import JsonResponse
from .models import Presentation, Status
from common.json import ModelEncoder
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

class PresentationListEncoder(ModelEncoder):
    model =  Presentation
    properties = [
        "title",
    ]

    def get_extra_data(self, o):
        return { "status": o.status.name }

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return { "status": o.status.name }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse({"presentations": presentations},
                            encoder=PresentationListEncoder)

    else:
        content = json.loads(request.body)

    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Conference Name"},
            status=400,
        )

    presentations = Presentation.create(**content)
    return JsonResponse(
        presentations,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count,_ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(name=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Conference Not Registered"},
                status=400,
        )

        try:
            if "status" in content:
                status = Status.objects.get(name=content["status"])
                content["status"] = status
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Conference Not Found"},
                status=400,
        )

        Presentation.objects.filter(id=id).update(**content)

        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
    )
